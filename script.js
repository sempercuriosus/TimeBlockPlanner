
$(function ()
{
  // This is a "Document Ready" Indicator, and lets the JS code know all required elements are now ready to be used.

  // #region NOW
  //

  let now = {
    "current": "",
    "today": "",
    "hour12": 0,
    "hour24": 0,
    "stringFormat": "ddd DD-MMM-YYYY [at] HH:mm:ss",
    "formatted": "",
    "dayFormat": "dddd | DD MMM, YYYY",

    updateNow ()
    {
      // getting the current time
      this.current = dayjs();
      this.formatNow();
      this.getCurrentDay();
      this.getCurrentHour();
    },

    formatNow ()
    {
      this.formatted = this.current.format(this.stringFormat);
    },

    getCurrentDay ()
    {
      this.today = this.current.format(this.dayFormat);
    },

    getCurrentHour ()
    {
      this.hour24 = parseInt(this.current.format("HH00"));
      this.hour12 = parseInt(this.current.format("hh"));
    },
  };

  setInterval(now.updateNow.bind(now), 1000);

  //
  // #endregion

  // #region SAVE BUTTON EVENT LISTENERS
  //


  function addSaveOnClick ()
  { //    [ about ] : 
    // console.log("[ saveButton ] : called");

    let saveButtons = $(".saveBtn");

    saveButtons.each(function (index)
    {
      let button = $(saveButtons[index]);
      let btnParent = $(button).parent();
      let parentID = $(btnParent).attr("id");

      button.on("click", function ()
      {
        saveTimeBlockPlan(parentID);
        showNotification();
      });
    });

  }; //  [ end : saveButton ]

  function showNotification ()
  { //    [ about ] : show a confirmation to the user that the item was saved
    // console.log("[ showNotification ] : called");
    $('#notification_area').addClass('unhide');

    // console.log($('#notification_area'));

    // Timeout to remove 'show' class after 5 seconds
    setTimeout(function ()
    {
      $('#notification_area').removeClass('unhide');
    }, 7000);
  }; //  [ end : showNotification ]

  function saveTimeBlockPlan (id)
  { // [ about ] : save the text from the given line to local storage
    // console.log("[ save ] : called");

    let objectKey = id;
    let item = $("#" + id);
    let value = item.children("textarea").val();
    let name = timeBlockPlanObj.name;

    setLocalStorage(name, objectKey, value);
  } // [ end : save ]

  // #endregion

  // #region SAVE TO LOCAL STORAGE
  //

  let timeBlockPlanObj = {
    "name": "timeBlockPlan",
    "times": {
      // "hour-0000": "",
      // "hour-100": "",
      // "hour-200": "",
      // "hour-300": "",
      // "hour-400": "",
      // "hour-500": "",
      // "hour-600": "",
      // "hour-700": "",
      // "hour-800": "",
      "hour-900": "",
      "hour-1000": "",
      "hour-1100": "",
      "hour-1200": "",
      "hour-1300": "",
      "hour-1400": "",
      "hour-1500": "",
      "hour-1600": "",
      "hour-1700": "",
      // "hour-1800": "",
      // "hour-1900": "",
      // "hour-2000": "",
      // "hour-2100": "",
      // "hour-2200": "",
      // "hour-2300": "",
    },
  };

  function initLocalStorage ()
  { // [ about ] : init the local storage
    // console.log("[ initLocalStorage ] : called");

    let timeBlock = timeBlockPlanObj;
    let keyName = timeBlock.name;

    let dataFromLocalStorage = getLocalStorage(keyName);

    if (!dataFromLocalStorage)
    {
      // console.log("Initialize : " + keyName);
      let dataToLocalStorage = timeBlock;
      setLocalStorage(keyName, dataToLocalStorage);
    }
    else
    {
      // console.log("Load Workday");
      loadWorkday();
    }

    // console.log("INIT LS DONE", "Data returned => ", getLocalStorage(keyName));
  } // [ end : initLocalStorage ]

  function getLocalStorage (keyName)
  { // [ about ] : gets the string from local storage and returns from JSON.parse()
    // console.log("[ getLocalStorage ] : called");

    if (keyName)
    {
      let dataFromLocalStorage = localStorage.getItem(keyName);
      let data = JSON.parse(dataFromLocalStorage);

      return data;
    }

  } // [ end : getLocalStorage ]

  function setLocalStorage (localStorageKeyName, objectKey, value)
  { // [ about ] : localStorageKeyName - local storage key; objectKey - a given object's key; value - what is going to be stored;
    // console.log("[ setLocalStorage ] : called");
    // console.log(localStorageKeyName, objectKey, value);

    if (localStorageKeyName && objectKey)
    {
      let updated = syncLocalStorageObject(objectKey, value);

      let dataToLocalStorage = JSON.stringify(updated);
      // console.log(dataToLocalStorage);

      localStorage.setItem(localStorageKeyName, dataToLocalStorage);
      // console.log("Local Storage Set");
    }
  } // [ end : setLocalStorage ]


  function syncLocalStorageObject (keyName, modifiedValue)
  { //    [ about ] : syncs the object and then updates local storage
    console.log("[ syncLocalStorageObject ] : called");

    if (keyName)
    {
      let timeBlock = timeBlockPlanObj;
      let timeBlockValues = timeBlock.times;

      for (key in timeBlockValues)
      {
        if (keyName == key)
        {
          // console.log(keyName, modifiedValue);
          timeBlockValues[key] = modifiedValue;
        }
      }

      return timeBlock;
    }
  }; //  [ end : syncLocalStorageObject ]


  function loadWorkday ()
  { //    [ about ] : loads, from local storage the work for the day
    // console.log("[ loadWorkday ] : called");

    let timeBlockHours = $(".time-block");
    let timeBlock = timeBlockPlanObj.name;
    let taskList = getLocalStorage(timeBlock);

    // console.log(taskList);

    $(timeBlockHours).each(function ()
    {
      let id = $(this).attr("id");
      let idPlusClass = "#" + id + " .description";
      let value = taskList.times[id];

      $(idPlusClass).val(value);

      // console.log("Current ", id, value);
    });

  }; //  [ end : loadWorkday ]

  //
  // #endregion

  // #region UPDATE CLASSES FOR CURRENT TIME
  //

  function updateHourClasses ()
  { // [ about ] : updates the hour classes to reflect the current time
    // console.log("[ updateHourClasses ] : called");
    let currentHour24 = now.hour24;
    let currentHour12 = now.hour12;
    let timeBlockHours = $(".time-block");

    // console.log(currentHour24, currentHour12);

    timeBlockHours.each(function (index)
    {
      let item = $(timeBlockHours[index]); // using $() to return a jquery obj and not just the element
      let blockId = item.attr("id");
      let blockHour = parseInt(blockId.split("-")[1]);

      if (currentHour24 < blockHour)
      {
        item.addClass(" future");
      } else if (currentHour24 == blockHour)
      {
        item.addClass(" present");
      } else
      {
        item.addClass(" past");
      }
    });
  } // [ end : updateHourClasses ]

  setInterval(updateHourClasses, 1000 * 60 * 10);

  //
  // #endregion

  // #region UPDATE THE CURRENT DATE
  //

  function updateDateDisplay ()
  { // [ about ] : displays the current date
    // console.log("updateDayDisplay", " : called");
    let currentDateEl = $("#currentDay");

    currentDateEl.text(now.today);
  } // [ end : updateDay ]

  setInterval(updateDateDisplay, 1000 * 60); // update on the minute. not sure what the "real" precision should be but to update a day I think a call on the min should be fine.

  //
  // #endregion

  // #region INIT
  //
  function INIT ()
  { // [ about ] : INIT
    // setting the initial value of now; if not done then now would only update 1 sec after load
    now.updateNow();
    updateDateDisplay();
    updateHourClasses();
    addSaveOnClick();
    initLocalStorage();

  } // [ end : INIT ]

  //
  // #endregion

  // Interval to call functions as needed for tests.
  // setInterval(, 3000);

  INIT();
});
