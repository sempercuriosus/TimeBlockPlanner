# TimeBlockPlanner

<!-- Title  -->

---

## Table of Contents

<!-- Table of Contents -->

- [About The Project](#about_project)
- [Getting Started](#getting_started)
- [Deployment Location](#deployment_location)
- [Challenges](#challenges)
- [Author Credit](#author_credit)
- [Acknowledgments](#acknowledgments)
- [Resources](#resources)
- [Final Note](#final_note)

---

## About The Project <a id="about_project"></a>

<!-- About the Project -->

This is a calendar app that allowing a user to save an event for each hour of a typical working day (9am–5pm)

This app was designed to help solve this problem

```
AS AN employee with a busy schedule
I WANT to add important events to a daily planner
SO THAT I can manage my time effectively
```

### Notes About Development/ Design Choices

- When being given starter code, it should be noted not to change more than what is required of the given project scope. In the real world this could break the existing functionality of a project, and any additional/unscoped changes should just not be made.
  - With that, I see that we have examples supplied in the HMTL showing the application of classes to change the colors, given the time of the day, but I do not see we are required to use those examples _exactly_ as they are.
  - As the Developer of this applicaiton I am making the choice to use semantic elements instead of the `<div>` tags provided in the examples.
- I am not modifying the structure of the starter code as I did last week.
  - I was given a `Develop` directory containing:
    - index.html
    - script.js
    - style.css
  - There were no sub directories for `Style` or `Script`, and when I did do that I had marks taken away.

## Acceptance Criteria

- [x] WHEN I open the planner THEN the current day is displayed at the top of the calendar
- [x] WHEN I scroll down THEN I am presented with time blocks for standard business hours of 9am to 5pm
- [x] WHEN I view the time blocks for that day THEN each time block is color-coded to indicate whether it is in the past, present, or future
- [x] WHEN I click into a time block THEN I can enter an event
- [x] WHEN I click the save button for that time block THEN the text for that event is saved in local storage
- [x] WHEN I refresh the page THEN the saved events persist
### Built With

<!-- Built With -->

- HTML
- CSS
- Bootstrap
- Javascript
- JQuery

### Features

<!-- Features -->

- There is a layout of the current day, from 0900 to 1700, for a typical working day.
- The current time block is highlighted to emphasize the current hour, along with the past and future blocks being presented with their own styling
- Users may input in tasks they want to accomplish at a specific time throughout the day.
- Items are saved to local storage

---

## Getting Started <a id="getting_started"></a>

<!-- Getting Started  -->

### Prerequisites & Dependencies

<!-- Prerequisites & Dependencies-->

- [Bootstrap Version 5.1.3](https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css)
- [Font Awesome](https://use.fontawesome.com/releases/v5.8.1/css/all.css)
- [Google Fonts](https://fonts.googleapis.com/css?family=Open+Sans&display=swap)
- [JQuery Version 3.4.1](https://code.jquery.com/jquery-3.4.1.min.js)
- [DayJS](https://cdn.jsdelivr.net/npm/dayjs@1.11.3/dayjs.min.js)

### Installation

<!-- Installation -->

No installation required! This is a web application and can be used anywhere.

### Usage

<!-- Usage -->

How to make your schedule:

1. Select a time, in which, you will enter in details about what you want to do
2. Click "Save" 
3. A confirmation will come up showing the item was saved

#### Configurables

<!-- Configurables -->

Nothing to configure in this application

---

## Deployment Location <a id="deployment_location"></a>

<!-- Deployment Location -->
The application can be found [here](https://sempercuriosus.github.io/TimeBlockPlanner/)

Here are some examples of the application
- The Current Time Block is in **Red**
- The Upcoming Time Block(s) is/are in **Green**
- The Previous Time Block(s) is/are in **Grey** 

### Main Page 
![Main Page Image](readme_examples/main_page.png)

### Time Has Passed 
![Previous Tasks Image](readme_examples/past_tasks.png)

### Save Confirmation
![Save Message Example](readme_examples/save_confirm.png)

---

## Challenges <a id="challenges"></a>

<!-- Challenges -->

1. Working with Local Storage
   - This was giving me issues when setting and getting values, I think that I was trying to do too many things at one time, and got my values confused initially. 
   - I ultimately took a step back and clarified what I needed, and was able to implement exactly what I wanted to.
2. Making `now {...}` update at an interval
   - Xpert pointed out `bind()` and the fact that `this'` context is lost when using the setInterval.
   - By using `bind()` you can ensure that the context is `now {}` within setInterval's callback
     - `setInterval(now.updateNow.bind(now), 1000);`


Something that I am extremely proud of myself was for figuring out how to make the section of code for my `now` object to work, save the `bind()` note. I think that is a really neat example using a third-party integration, and really customizing it for what I was looking for.

---

## Author Credit <a id="author_credit"></a>

<!-- Author Credits -->

Eric Hulse (Semper Curiosus)

---

## Acknowledgments <a id="acknowledgments"></a>

<!-- Acknowledgments -->

This was built as a class project with starter code. 
- The HTML was mostly from the starter code, but I did change the original div to be a 
section. See that in [About The Project](#about_project)
- The CSS is almost all from that example and styled with bootstrap otherwise.
- The JavaScript is 100% mine. 

---

## Resources <a id="resources"></a>

<!-- Resources -->
- [Day JS Docs](https://day.js.org/docs/en/display/format)
- [JQuery Docs](https://api.jquery.com/)
- [Local Storage Article](https://blog.alexdevero.com/web-storage-api-local-session-storage/)
- [Stack Overflow Local Storage Article](https://stackoverflow.com/questions/13092715/localstorage-use-getitem-setitem-functions-or-access-object-directly#13093116)
- [Git Merge Issue](https://unix.stackexchange.com/questions/181280/how-to-exit-a-git-merge-asking-for-commit-message)
    - I got stuck in a merge message 
---

## Final Note <a id="final_note"></a>

<!-- Final Note -->
This project was more of a real world example, I thought, and working with local storage was frustrating at first. That was 100% my own doing, but I was able to figure it out in the end and see what I was doing wrong.

---

